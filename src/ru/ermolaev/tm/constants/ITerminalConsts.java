package ru.ermolaev.tm.constants;

public interface ITerminalConsts {

    String HELP = "HELP";

    String ABOUT = "ABOUT";

    String VERSION = "VERSION";

}
